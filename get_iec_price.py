from typing import Any, List, Dict, NewType
import requests
from simple_term_menu import TerminalMenu

Optional_for = NewType("Optional for", Any)

list_of_size = [
    "40",
    "3X25",
    "3X40",
    "3X63",
    "3X80",
    "3X100",
    "3X125",
    "3X160",
    "3X200",
    "3X250",
    "3X315",
    "3X400",
    "3X500",
    "3X630",
    "3X800",
    "3X910",
]
list_of_size2 = [
    "25",
    "35",
    "40",
    "3X25",
    "3X35",
    "3X40",
    "3X50",
    "3X63",
    "3X80",
    "3X100",
    "3X125",
    "3X160",
    "3X200",
    "3X250",
    "3X315",
    "3X400",
    "3X500",
    "3X630",
    "3X800",
]
url = "https://masaapi-wa.azurewebsites.net/calculator/"

steps = {
    "question": "למה המבנה משמש?",
    "מבנה מגורים": {
        "question": "איזה סוג חיבור קיים?",
        "חיבור חדש צמודי קרקע": {
            "url": "sumTzamod",
            "requests": {
                "question": "כמות חיבורים?",
                "בית בודד": {
                    "request": {
                        "connectionType": "A",
                        "connectionSize1": {
                            "question": "גודל חיבור מבוקש",
                            "type": List,
                            "value": list_of_size[:5],
                        },
                        "connectionSize2": "",
                    }
                },
                "דו  משפחתי": {
                    "request": {
                        "connectionType": "B",
                        "connectionSize1": {
                            "question": "גודל חיבור מבוקש",
                            "type": List,
                            "value": list_of_size[:5],
                        },
                        "connectionSize2": "",
                    }
                },
                "שני חיבורים בפילר לשני מונים": {
                    "request": {
                        "connectionType": "C",
                        "connectionSize1": {
                            "question": "גודל חיבור מבוקש",
                            "type": List,
                            "value": list_of_size[:5],
                        },
                        "connectionSize2": {
                            "question": "גודל חיבור שני מבוקש",
                            "type": List,
                            "value": list_of_size[:5],
                        },
                    }
                },
            },
        },
        "חיבור חדש בנייני מגורים ": {
            "url": "sumAppartmentHouses",
            "request": {
                "publicConnectionSize": {
                    "question": "גודל חיבור ציבורי",
                    "type": List,
                    "value": list_of_size,
                },
                "connections": {
                    "question": "גודל החיבור המבוקש",
                    "type": Optional_for,
                    "value": {"for": "size", "in": "amount"},
                    "for": list_of_size[:5],
                    "in": {
                        "value": ["1", "50"],
                        "type": range,
                        "question": "מספר החיבורים הרצוי לגודל {size} בין {min} לבין {max}",
                    },
                },
                "counterInstallationType": {
                    "question": "איפה להתקין את המונים",
                    "type": Dict,
                    "value": {
                        "ריכוז בכניסה לבנין": 1,
                        "ריכוז בכל קומה": 2,
                        "פיזור מונים": 3,
                    },
                },
            },
        },
        "הגדלת חיבור קיים ": {
            "url": "sumExtensionOfExsiting",
            "request": {
                "existingConnectionSize": {
                    "question": "גודל חיבור קיים ",
                    "type": List,
                    "value": list_of_size2,
                },
                "requestedConnectionSize": {
                    "question": "גודל חיבור מבוקש",
                    "type": List,
                    "value": list_of_size,
                },
            },
        },
    },
    "מבנה תעשייה ומסחר": {
        "question": "איזה סוג חיבור קיים?",
        "חיבור בפילר/ מערכות מנייה": {
            "url": "sumIndustriealPiler",
            "requests": {
                "question": "כמות חיבורים?",
                "חיבור אחד בפילר/ מערכת מנייה": {
                    "request": {
                        "connectionType": "A",
                        "connectionSize1": {
                            "question": "גודל חיבור מבוקש",
                            "type": List,
                            "value": list_of_size[:5],
                        },
                        "connectionSize2": "",
                    }
                },
                "חיבור אחד בפילר לשני מונים": {
                    "request": {
                        "connectionType": "B",
                        "connectionSize1": {
                            "question": "גודל חיבור מבוקש",
                            "type": List,
                            "value": list_of_size[:5],
                        },
                        "connectionSize2": "",
                    }
                },
                "שני חיבורים בפילר לשני מונים": {
                    "request": {
                        "connectionType": "C",
                        "connectionSize1": {
                            "question": "גודל חיבור מבוקש",
                            "type": List,
                            "value": list_of_size[:5],
                        },
                        "connectionSize2": {
                            "question": "גודל חיבור מבוקש",
                            "type": List,
                            "value": list_of_size[:5],
                        },
                    }
                },
            },
        },
        "חיבור למבנה מסחרי/ תעשייתי": {
            "url": "sumIndustriealBuilding",
            "request": {
                "connections": {
                    "question": "גודל החיבור המבוקש",
                    "type": Optional_for,
                    "value": {"for": "size", "in": "amount"},
                    "for": list_of_size,
                    "in": {
                        "value": ["1", "50"],
                        "type": range,
                        "question": "מספר החיבורים הרצוי לגודל {size} בין {min} לבין {max}",
                    },
                },
                "counterInstallationType": {
                    "question": "איפה להתקין את המונים",
                    "type": Dict,
                    "value": {
                        "ריכוז בכניסה לבנין": 1,
                        "ריכוז בכל קומה": 2,
                        "פיזור מונים": 3,
                    },
                },
            },
        },
        "הגדלת חיבור": {
            "url": "sumIndustriealExtensionOfExsiting",
            "request": {
                "existingConnectionSize": {
                    "question": "גודל חיבור קיים",
                    "type": List,
                    "value": list_of_size2,
                },
                "requestedConnectionSize": {
                    "question": "גודל חיבור מבוקש",
                    "type": List,
                    "value": list_of_size,
                },
                "counterInstallationType": {
                    "question": "איפה להתקין את המונים",
                    "type": Dict,
                    "value": {
                        "פילר בגבול מגרש": 1,
                        "ריכוז בכניסה לבניין": 2,
                        "ריכוז בכל קומה": 3,
                        "פיזור מונים": 4,
                    },
                },
            },
        },
    },
}


def terminal_menu(menu_entries, title, as_index=None, multi_select=False):
    if title[0].encode() >= "א".encode() and title[0].encode() <= "ת".encode():
        title = title[::-1]
    for index, menu_entrie in enumerate(menu_entries):
        if (
            menu_entries[0].encode() >= "א".encode()
            and menu_entries[0].encode() <= "ת".encode()
        ):
            menu_entries[index] = menu_entrie[::-1]

    if multi_select:
        terminal_menu = TerminalMenu(
            menu_entries, title=title, show_multi_select_hint=True, multi_select=True
        ).show()
        if as_index:
            return terminal_menu
        return list(map(lambda x: menu_entries[x], terminal_menu))
    terminal_menu = TerminalMenu(menu_entries, title=title).show()
    if as_index:
        return terminal_menu
    return menu_entries[terminal_menu]


def first_step_cli(steps):
    url = None
    while True:
        if "url" in steps:
            url = steps["url"]
        if "request" in steps:
            break
        if "requests" in steps:
            steps = steps["requests"]

        keys = [x for x in steps.keys() if x != "question"]
        answer = terminal_menu(menu_entries=keys, title=steps["question"])
        steps = steps[answer[::-1]]

    steps["url"] = url
    return steps


def process_node(node, additional=None):
    for key in node:
        if isinstance(node[key], dict):
            node[key] = process_node(node[key])
        elif "type" in node:
            if node["type"] is range:
                node = input(
                    f'{node["question"].format(size=additional[::-1], min=min(node["value"])[::-1],max=max(node["value"])[::-1])[::-1]}: '
                )
            elif node["type"] is List:
                node = terminal_menu(menu_entries=node["value"], title=node["question"])
            elif node["type"] is Optional_for:
                values = terminal_menu(
                    menu_entries=node["for"], title=node["question"], multi_select=True
                )
                new_node = []
                for value in values:
                    new_node.append(
                        {
                            node["value"]["for"]: value,
                            node["value"]["in"]: process_node(node["in"], value),
                        }
                    )
                node = new_node
            elif node["type"] is Dict:
                node = node["value"][
                    terminal_menu(
                        menu_entries=list(node["value"].keys()),
                        title=node["question"],
                    )[::-1]
                ]
            return node
    return node


def request_prices(request_arg):
    return requests.post(url + request_arg["url"], json=request_arg["request"]).json()


if __name__ == "__main__":
    raw_request = first_step_cli(steps)
    raw_request["request"] = process_node(raw_request["request"])
    prices = request_prices(raw_request)
    print(prices["ab"], "עלות החיבור:"[::-1])
    print(prices["h"], "עלות החפירה:"[::-1])
    print(prices["priceNoTax"], 'סה"כ לפני מע"מ:'[::-1])
    print(
        round(prices["priceNoTax"] * (prices["tax"] / 100), 2),
        f'{prices["tax"]}' + "%" + 'מע"מ '[::-1],
    )
    print(round(prices["priceWithTax"], 2), 'סה"כ לחיוב'[::-1])
