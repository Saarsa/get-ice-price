# Get Price for Order or Increase Power Connection

This GitLab repository contains a Python script that allows you to obtain the current price of electricity in Israeli new shekels from the Israel Electric Corporation (ICE). The script can be used to help determine the cost of increasing the power connection for a home or business.

## Installation

1. Clone this GitLab repository to your local machine.
2. Install the required packages listed in the `requirements.txt` file by running the command: `pip install -r requirements.txt`

## Usage

1. Open a terminal window and navigate to the directory where you cloned this repository.
2. Run the command: `python get_ice_price.py`
3. The current price of electricity in Israeli new shekels will be displayed in the terminal.

## Contributing

Contributions to this repository are welcome. If you would like to contribute, please follow these steps:

1. Fork this repository to your own account.
2. Create a new branch with a descriptive name (`git checkout -b my-new-feature`).
3. Make changes or additions to the code.
4. Commit your changes with a descriptive commit message (`git commit -m "Add some feature"`).
5. Push your changes to your forked repository (`git push origin my-new-feature`).
6. Open a merge request on this repository and describe your changes.

## Issues

If you have any questions or issues with this script, please feel free to open an issue on this repository.

## Todo

- [ ] Check for bidi

## License

This repository is licensed under the [MIT License](https://opensource.org/licenses/MIT). Feel free to use the code in this repository for your own projects.

## Disclaimer

This Python script is not affiliated with the Israel Electric Corporation in any way. The script is intended for informational purposes only, and the accuracy of the price information cannot be guaranteed.
